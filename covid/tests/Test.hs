{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Test.Tasty
import Test.Tasty.QuickCheck as QC
--import Test.Tasty.SmallCheck as SC

import Covid
import Data.List (tails)
import Data.AEq

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [properties]

properties :: TestTree
properties = testGroup "Properties" [propsPairDiff, propsMAvg]

propsPairDiff :: TestTree
propsPairDiff = testGroup "pairDiff"
  [ QC.testProperty "shortens length by 1" $
      \(NonEmpty xs) -> length (pairDiff (xs :: [Int])) == length xs - 1,
    QC.testProperty "correctness of pairwise difference" $
      \(NonEmpty xs) -> scanl (+) (head xs :: Int) (pairDiff xs) == xs
  ]

windowSum :: Num c => Int -> [c] -> [c]
windowSum n xs = takeLengthOf (drop (n-1) x0s) (windows' x0s)
  where
    x0s = replicate (n-1) 0 ++ xs
    windows' = map (sum . take n) . tails
    takeLengthOf = zipWith (const id)

propsMAvg :: TestTree
propsMAvg = testGroup "mAvg"
  [ QC.testProperty "same length" $
      \(n :: Int, xs :: [Double]) -> n > 0 && n <= length xs ==> length (mAvg n xs) == length xs,
    QC.testProperty "length 1 has no effect" $
      \(xs :: [Double]) -> mAvg 1 xs == xs,
    QC.testProperty "length n sums match" $
      \(n :: Int, xs :: [Double]) -> n > 0 && n <= length xs ==> and (zipWith (~==) (map (fromIntegral n*) (mAvg n xs)) (windowSum n xs))

  ]

