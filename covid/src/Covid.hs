{-# LANGUAGE OverloadedStrings #-}

module Covid where

import qualified Data.ByteString.Char8 as BS
import qualified Data.Vector as V
import Data.Csv ( (.:), decodeByName, FromNamedRecord(..), FromField (parseField) )
import Data.Time.Format ( defaultTimeLocale, parseTimeM )
import Data.Time.Calendar ( Day )
import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Diagrams (toFile)
import Network.HTTP.Req

data CaseDay = CaseDay
    { date   :: !Day
    , cases  :: !Int
    , deaths :: !Int
    }

instance FromField Data.Time.Calendar.Day where
  -- New York Times dates are in this format
  parseField = parseTimeM True defaultTimeLocale "%Y-%m-%d" . BS.unpack

instance FromNamedRecord CaseDay where
    parseNamedRecord r = CaseDay <$> r .: "date" <*> r .: "cases" <*> r .: "deaths"

-- | 'pairDiff' computes the pairwise difference between adjacent elements
pairDiff :: Num a => [a] -> [a]
pairDiff = zipWith (flip (-)) <*> tail

-- | 'avgDailyData' takes cumulative data, 
-- processes it to average daily data, and pairs it with the date
avgDailyData :: Fractional b1 => (b2 -> b1) -> (CaseDay -> b2) -> [CaseDay] -> [(Day, b1)]
avgDailyData valueInst f cs = zip (map date cs) avg
  where
    daily = pairDiff $ map (valueInst . f) cs
    avg = mAvg 7 daily

-- | 'filterData' removes entries where the X-value is 0.
-- This avoids issues when taking the log.
filterData :: Eq b => Num b => [(a, b)] -> [(a, b)]
filterData = filter ((0/=) . snd)

-- | 'mAvg' computes the moving average over a window of length 'n'
mAvg :: Fractional a => Int -> [a] -> [a]
mAvg n xs =
  case xs of
    [] -> []
    _ -> mAvgH x0s

  where x0s = replicate (n-1) 0 ++ xs
        mAvgH ys
          | length ys < n = []
          | otherwise = (sum (take n ys) / fromIntegral n) : mAvgH (tail ys)


-- | 'plotCases' constructs a plot of cases and deaths per day from the data
plotCases :: (Eq y, Fractional y, PlotValue y) => (Int -> y) -> V.Vector CaseDay -> EC (Layout Day y) ()
plotCases valueInst covidData =
  do  layout_title .= "Logarithmic Covid Cases per Day (" ++ show latestDate ++ ")"
      setColors [opaque blue, opaque red]
      plot (line "cases" [caseData])
      plot (line "deaths" [deathData])
  where
      dataList = V.toList covidData
      caseData = (filterData . avgDailyData valueInst cases) dataList
      deathData = (filterData . avgDailyData valueInst deaths) dataList
      latestDate = maximum (map date dataList)

-- | 'getRequest' builds a GET Request for the url
getRequest :: MonadHttp m => Url scheme -> m LbsResponse
getRequest url = req GET url NoReqBody lbsResponse mempty

realMain :: IO ()
realMain =
  do  response <- runReq defaultHttpConfig $ getRequest usDataUrl
      let casesEither = snd <$> decodeByName (responseBody response)
      let valueInst = fromIntegral :: Int -> LogValue
      let plotter = toFile def "log_covid.svg" . plotCases valueInst

      either putStrLn plotter casesEither

  where
      usDataUrl  = https "raw.githubusercontent.com" /: "nytimes" /: "covid-19-data" /: "master" /: "us.csv"
